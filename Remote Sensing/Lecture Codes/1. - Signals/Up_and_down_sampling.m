%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DOWN AND UP SAMPLING OF AN AUDIO SIGNAL
% We downsample the original signal (i.e. reducing bitrate), then we
% reconstruct the original signal by interpolation w/ outstanding accuracy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if 0
    clear, clc
    [y,Fs] = audioread('Tous les memes.m4a');
    % cut the length of the song
    y = y(7.5e5:12e5,:);
    [N,tr] = size(y);
    whos
    player = audioplayer(y,Fs);
    play(player);
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOWNSAMPLING
dws = 8 % downsampling factor
% NO PRE-FILTERING
ys = y(1:dws:end,:);
%soundsc(ys,Fs/dws);
% RMK.: A factor of 8 is a very strong downsampling which, in the absence
% of filtering, will inevitably yield aliasing, which translates into a
% distorted audio clip

% NO PRE-FILTERING
% Information is lost due to pre-filtering, but at least this avoids
% overlapping replicas (i.e. no aliasing), hence no distortions
h = fir1(1e4,1/dws); % see fir1 on Matlab doc
                     % N.B.: FIR = Finite Impulse Response
% expressed as a column vector since y is a Ntx2 matrix
h = h(:);

% Spectral analysis
Nf = 2^ceil(log2(N));
Y = fftshift(fft(y,Nf,1));
H = fftshift(fft(h,Nf));
f = (-Nf/2:Nf/2-1)/Nf;
figure
H = H/max(H)*max(Y(:)); % scaling is just for visualization
plot(f,abs(Y),f,abs(H)), grid
xlabel('normalized frequency')

% Filtering
yf = conv2(y,h,'same');

figure
ind = 20000 + (1:100);
plot(ind,yf(ind,1),ind,y(ind,1)), grid
legend('filtered signal','original signal')

% downsampling the filtered signal
yfs = yf(1:dws:end,:);
%soundsc(yfs,Fs/dws);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% UP-SAMPLING (INTERPOLATION)
% We pick one sample every dws ones, while in the middle between two
% successive (down)samples we pad everything w/ zeros (as many as the
% dropped samples)
ys = zeros(size(y)); % same size as the original signal
ys(1:dws:end,:) = yfs;
figure
plot(ind,yf(ind,1),ind,ys(ind,1),'o'), grid

% reconstruction/up-sampling/interpolation
h = h/max(h)
yr = conv2(ys,h,'same'); 

% Spectral analysis
Ys = fftshift(fft(ys,Nf,1));
Yr = fftshift(fft(yr,Nf,1));
figure
subplot(2,1,1), plot(f,abs(Ys)), grid
xlabel('normalized frequency')
title('down-sampled signal')
subplot(2,1,2), plot(f,abs(Yr)), grid
xlabel('normalized frequency')
title('reconstructed signal')

figure
plot(ind,yf(ind,1),ind,yr(ind,1)), grid
legend('filtered signal','reconstructed signal')
%soundsc(yr,Fs);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%