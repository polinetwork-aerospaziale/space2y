%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Signal
fs = 8192;
dt = 1/fs;
t = 0:dt:1-dt;
Nt = length(t);
f0 = 440+pi/10;
s = exp(1i*2*pi*f0*t');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fourier Transform
Nf = 2^ceil(log2(Nt));
f_ax = (-Nf/2:Nf/2-1)/Nf/dt; % frequency axis
S = fftshift(fft(s,Nf));
figure, subplot(2,1,1), plot(f_ax,abs(S),'o-'), grid on, hold on
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('Signal')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation  of the peak
[Sm,k] = max(S);
f0_est = f_ax(k)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cancellation by zeroing samples around the peak
L = 2
S_clean = S;
S_clean(k+(-L:L)) = 0;
subplot(2,1,1),  plot(f_ax,abs(S_clean),'o-'), grid on, hold on
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
% antitrasform
ddd = ifft(ifftshift(S_clean));
s_clean = real(ddd(1:Nt));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FT on the same number of points
S_clean = fftshift(fft(s_clean,Nf));
subplot(2,1,2), plot(f_ax,abs(S_clean),'ko-'), , grid on, hold on
xlim(f0+[-10 10])
title('Cancellation')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Let's see what happen if we zoom in
Nf = 8*Nf % oversampling in the frequency domain
f_ax = (-Nf/2:Nf/2-1)/Nf/dt;
S_clean = fftshift(fft(s_clean,Nf));
subplot(2,1,2), plot(f_ax,abs(S_clean),'r'), 
xlim(f0+[-10 10])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
