%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR ESTIMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MSE = E[ (x^ - x_true)^2 ] = Var(x^) - (E[(x^ - x_true)])^2
% RMSE = sqrt(MSE)
% Ai*A = check bias. The inversion unbiased only if Ai*A = 1
row_names{1} = 'Sample rmse';
row_names{2} = 'Theoretical rmse';
row_names{3} = 'Ai*A';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA GENERATION
N = 10;
M = 4;
A = randn(N,M); % model
Ne = 1e5; % number of experiments

parameter_covariance_model = 2;
noise_covariance_model = 2;

% Unknowns
switch parameter_covariance_model
    case 1 % uncorrelated parameters
        Cx = eye(M);
    case 2 % correlated parameters
        pho_x = 0.9;
        Cx = (1-pho_x)*eye(M) + pho_x*ones(M,M); 
    otherwise
end
% Generation of correlated random unknowns
[U,S,~] = svd(Cx);
Vx = U*sqrt(S); % Cx = Vx*Vx'
x_true = Vx*randn(M,Ne); % random parameters

% Noise
switch noise_covariance_model
    case 1 % uncorrelated noise
        Cw = eye(N);
    case 2 % correlated noise
        pho_w = 0.9;
        Cw = (1-pho_w)*eye(N) + pho_w*ones(N,N); 
    otherwise
end
% Generation of correlated noise
[U,S,~] = svd(Cw);
Vw = U*sqrt(S); % Cw = Vw*Vw'
w = Vw*randn(N,Ne); % noise

%
figure
subplot(3,4,1), imagesc(abs(Cx),[0 1]), colorbar
title('Parameter covariance')
subplot(3,4,5), imagesc(abs(Cw),[0 1]), colorbar
title('Noise covariance')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA
y = A*x_true + w;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION
for est_technique = 1:3
    switch est_technique
        case 1 % LS estimation
            Ai = inv(A'*A)*A';
            Est_Tech = 'LS';
        case 2 % BLUE
            Cwi = inv(Cw + 1e-14*eye(N)); % helps inversion of Cw when it's singular
            Ai = inv(A'*Cwi*A)*A'*Cwi;
            Est_Tech = 'BLUE';
        case 3 % MMSE (Bayesian)
            Cy = A*Cx*A' + Cw;
            Cyx = A*Cx;
            Ai = Cyx'*inv(Cy);
            Est_Tech = 'MMSE';
        otherwise
    end
    % Estimation
    x = Ai*y;
    % Estimation error
    err = x - x_true;
    % Evaluation of performance (rmse) assuming knowledge of x_true
    Ce_sample = 1/Ne*(err*err');
    % Theoretical evaluation under the assumption of unbiasedness (OK for parametric estimation)
    Ce_theoretical  = Ai*Cw*Ai';
    % Theoretical evaluation for MMSE (accounting for Cx)
    if est_technique == 3
        Ce_theoretical  = Cx - Cyx'*inv(Cy)*Cyx;
    end
    % Check if the inversion is unbiased in the sense of parametric
    % estimation
    ddd = Ai*A - eye(M);
    Bias(est_technique) = max(abs(ddd(:)))>1e-4;
    %
    subplot(3,4,est_technique+1), imagesc(abs(Ce_sample)), 
    title([Est_Tech ' sample covariance'])
    subplot(3,4,est_technique+5), imagesc(abs(Ce_theoretical))
    title([Est_Tech ' theoretical covariance'])
    subplot(3,4,est_technique+9), stem(sqrt(diag(Ce_sample))), grid
    title([Est_Tech ' RMSE'])
end
Bias

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

