%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFLECTION FROM A FINITE-LENGTH SMOOTH/ROUGH SURFACE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all, close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SURFACE PARAMETERS
L = 3 % length [m]
% Also try e.g. L = 0.3 (small surf. w.r.t. wavelength) -> power is
% scattere everywhere!

sz = 0 % roughness [m]
% e.g. setting sz = 1 causes power to be scattered everywhere
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIELD PARAMETERS
c = 3e8; % propagation velocity  [m/s]
f0 = 1.2e9; % carrier frequency [Hz]
lambda = c/f0 % wavelength [m]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SURFACE COORDINATES
dyi = lambda/4; % spatial sampling % what happens if too coarse?
yi = (-L/2:dyi:L/2);
Ny = length(yi);
zi = sz*randn(size(yi));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SOURCE COORDINATES
ys = -30; 
zs = 30;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIELD AT THE SURFACE
% i.e. a spherical wave
R = sqrt( (ys-yi).^2 + (zs-zi).^2 );
Ei = exp(-1i*2*pi/lambda*R)./R;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCATTERED FIELD CALCULATION
dy = 1; % spatial sampling along y [m]
dz = dyi; % spatial sampling along z [m]
z = (-50:dz:50); % vertical axis [m]
y = (-50:dy:50); % horizontal axis [m]
[Z,Y] = ndgrid(z,y); % Matrices of (z,y) coordinates

% field calculation
Es = 0;
wb = waitbar(0,'computing scattered field...')
for n = 1:Ny
    waitbar(n/Ny,wb)
    Ry = sqrt((Y-yi(n)).^2 + (Z-zi(n)).^2); % distances from the n-th antenna
    Ey = Ei(n)*exp(-1i*2*pi/lambda*Ry)./Ry; % spherical wave from the n-th antenna
    Es = Es + Ey; % total field
end
close(wb)
E_db = 20*log10(abs(Es));
figure, imagesc(y,z,E_db), axis xy
colorbar, xlabel('y [m]'), ylabel('z [m]')
title('Scattered field intensity [dB]')
hold on, plot(ys,zs,'w.', 'MarkerSize', 15)
text(ys*0.95,zs, 'Illumination Source', 'FontSize', 16, 'Color', 'w')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



