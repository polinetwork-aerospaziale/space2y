%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEOMETRICAL DISTORTIONS IN RADAR GEOMETRY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear, clc

H = 1000; % flight height [m]

% ground range axis
y = linspace(100,1000,1000);
ym = mean(y);

% three different scenarios
scenario = 2
switch scenario
    case 1 % mild topographic slope - foreshortening
        z = exp(-((y-ym)/200).^2);
        z_max = 50;
    case 2 % mild topographic slope - layover
        z = exp(-((y-ym)/200).^2);
        z_max = 500;
    case 3 % building - layover
        z = rectpuls((y-ym)/50);
        z_max = 100;
    otherwise
end
z = z/max(z)*z_max;

% sensor-to-target distance
Sz = H; Sy = 0; % coordinates of the Radar
ry = sqrt((Sy-y).^2 + (Sz-z).^2); % distance (r as a function of y)

figure
subplot(3,1,1), plot(y,z), grid, ylim([0 z_max*1.2])
xlabel('ground range [m]'), ylabel('height [m]')
subplot(3,1,2), plot(ry,z), grid, ylim([0 z_max*1.2])
xlabel('range [m]'), ylabel('height [m]')

% NOTE: z cannot always be expressed as a function of r!

% try to express y and z as a function of r and see whay happens....
r = linspace(min(ry),max(ry),1000); % uniform r-axis
yr = interp1(ry,y,r); % y as a function of r
zr = interp1(ry,z,r); % z as a function of r
subplot(3,1,3), plot(yr,zr), grid, ylim([0 z_max*1.2])
xlabel('ground range [m]'), ylabel('height [m]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % in scenario 1, fig 1 = fig 3 bc the height is defined by an invertible funcion.
%  in scenatio 2 and 3  height is no more an invertible function



