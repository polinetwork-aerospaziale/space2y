close all
clear all
load HW_1_Problem_1.mat

% No. realizations  N           1x1              
% Pixels-x          Nx          1x1              
% Pixels-y          Ny          1x1              
% Coefficients-v    kv          1x4              
% Coefficients-z	kz          1x4              
% N Phase images    phi      650x1000x4 (Ny, Nx, N)        
% Noise variance    sw          1x1              
% True topography z          650x1000
% True displ. rate  v        650x1000


set(0,'defaultTextInterpreter','latex')
fontSize = 16;
set(0,'DefaultAxesFontSize', fontSize);

% bitmapres = 2000;
% ppi = bitmapres/10;
% [X,Y] = meshgrid((1:Nx),(Ny:-1:1));
x_px  = (1:Nx);      y_px = (1:Ny);


%% 0. Dataset

% Phase @ different realizations
h = figure;

myCMap = parula;

for k = 1:N
    subplot(2,2,k)
%   pcolor(X, Y, phi(:,:,k))
    imagesc(x_px, y_px, phi(:,:,k))
    colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
    axis equal; xlim([0,Nx]); ylim([0,Ny]);
    set(get(hcb,'Title'),'String','$\phi$ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
    title(sprintf('n = %i', k))
end

sgtitle('\textbf{Phase-Measurement Realizations}');
fileName = 'phaseMeasurement_realizations';
set(h, 'Units', 'normalized', 'OuterPosition', [0.26 0.13 0.48 0.74]);
% printFigure(h, fileName, 'png', bitmapres, ppi)
printFigure(h, fileName)



%% 1. & 3. BLUE Estimation
% 1. Propose an algorithm to estimate terrain topography and displacement rate (z, v)
% at any pixel location from the set of interferometric phases φ (x, y, n).
% 3. Apply the estimation algorithm on the data-set, observe the results and discuss them.


C_w = sw * eye(N);
A = [kz' kv'];

% Topography and Displacement-rate maps
Z = zeros(Ny,Nx);
V = zeros(Ny,Nx);

for i = 1:Ny
    for j = 1:Nx
    y = reshape(phi(i,j,:), 4,1);
    
    x_BLUE = (A'*C_w^-1*A)^-1*A'*C_w^-1 * y;
    Z(i,j) = x_BLUE(1);
    V(i,j) = x_BLUE(2);
    end
end


h=figure;

% Estimated topography
subplot(1,2,1)
imagesc(x_px, y_px, Z)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$z$ [m]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('Topography')


% Estimated displ. rate
subplot(1,2,2)
imagesc(x_px, y_px, V)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$v$ [mm/day]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('Displacement-Rate')

sgtitle('\textbf{BLUE Estimation}')

set(h, 'Units', 'normalized', 'OuterPosition', [.15 .5 .7 .5]);

fileName = 'BLUE_estimation';
printFigure(h, fileName)


% Low pass filter for De-Speckling
% fractional bandwidth along columns and rows
By = .1;
Bx = .1;
filtOrder = 20
filter_y = fir1(filtOrder,By);
filter_x = fir1(filtOrder,Bx);

t = conv2(Z,filter_x(:)','same'); % row filter
Z_fltr = conv2(t,filter_y(:),'same'); % column filter

t = conv2(V,filter_x(:)','same'); % row filter
V_fltr = conv2(t,filter_y(:),'same'); % column filter

h = figure;
subplot(1,2,1)
imagesc(x_px, y_px, Z_fltr)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$z$ [m]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('Topography')

subplot(1,2,2)
imagesc(x_px, y_px, V_fltr)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$v$ [mm/day]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('Displacement-Rate')

sgtitle('\textbf{De-Speckled BLUE Estimation}')

set(h, 'Units', 'normalized', 'OuterPosition', [.15 .0 .7 .5]);

fileName = 'BLUE_estimationFiltered';
printFigure(h, fileName)



%% 2. Theoretical Performance Evaluation

%Cw_inv = inv(C_w + 1e-14*eye(size(C_w))); % helps inversion of Cw when it's singular
Cw_inv = inv(C_w);
A_inv = inv(A'*Cw_inv*A)*A'*Cw_inv;

% Theoretical evaluation under the assumption of unbiasedness
% N.B. The diagonal elements correspond to the variances -> RMS = sqrt(variance)
RMS_theor  = sqrt(diag(A_inv*C_w*A_inv'))
% Evaluation of performance (rmse) assuming knowledge of x_true
RMS_empirical = [sqrt(mean(mean((Z-z).^2))); sqrt(mean(mean((V-v).^2)))]

% Check if the inversion is unbiased
unbiasednessCheck  = A_inv*A



%% Empirical Performance Evaluation
% 4. Assess estimation accuracy empirically based on the knowledge of true topography and displacement rate.


h = figure;
% True topography
subplot(1,2,1)
imagesc(x_px, y_px, z)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$z$ [m]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('True Topography')

% True displ. rate
subplot(1,2,2)
imagesc(x_px, y_px, v)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$v$ [mm/day]', 'Interpreter', 'latex', 'FontSize', fontSize);
title('True Displacement-Rate')

sgtitle('\textbf{Ground Truth}')

set(h, 'Units', 'normalized', 'OuterPosition', [.15 .5 .7 .5]);

fileName = 'groundTruth';
printFigure(h, fileName)




h = figure;

% Topography error
subplot(2,2,1)
imagesc(x_px, y_px, Z_fltr-z)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$z$ [m]', 'Interpreter', 'latex', 'FontSize', fontSize);
avg_err_zMagn = mean(mean(abs(Z_fltr-z)))
expVal_err_z = mean(mean((Z_fltr-z)))
avg_relErr_z = expVal_err_z / std(std(z))
title( sprintf('Topography Error \n ($E[\\varepsilon_z]$ = %.3f mm)', expVal_err_z) )


% Displacement-rate error
subplot(2,2,2)
imagesc(x_px, y_px, V_fltr-v)
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([0,Nx]); ylim([0,Ny]);
set(get(hcb,'Title'),'String','$v$ [mm/day]', 'Interpreter', 'latex', 'FontSize', fontSize);
avg_err_vMagn = mean(mean(abs(V_fltr-v)))
expVal_err_v = mean(mean((V_fltr-v)))
avg_relErr_v = expVal_err_v / std(std(v))
title(sprintf('Displacement-Rate Error \n ($E[\\varepsilon_v]$ = %.5f mm/day)', expVal_err_v))


% Spectral analysis
Nfy = 2^ceil(log2(Ny));
Nfx = 2^ceil(log2(Nx));
fy = (-Nfy/2:Nfy/2); % normalized frequency along columns
fx = (-Nfx/2:Nfx/2); % normalized frequency along rows
[FX,FY] = meshgrid(fx,fy);

subplot(2,2,3)
t = fft((Z_fltr-Z),Nfy,1); % Fourier transform along columns
t = fft(t,Nfx,2); % Fourier transform along rows
If = fftshift(t);
S = 1/(Nx*Ny) * abs(If).^2; % PSD
imagesc(fx, fy, 10*log10(abs(S)))
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([fx(1),fx(end)]); ylim([fy(1),fy(end)]);
xlabel('$f_x$ [Hz]'), ylabel('$f_y$ [Hz]')
title('PSD of z-error [dB]')




subplot(2,2,4)
t = fft((V_fltr-V),Nfy,1); % Fourier transform along columns
t = fft(t,Nfx,2); % Fourier transform along rows
If = fftshift(t);
S = 1/(Nx*Ny) * abs(If).^2; % PSD
imagesc(fx, fy,10*log10(abs(S)))
colormap(myCMap); shading interp; xlabel('X-pixels'); ylabel('Y-pixels'); hcb = colorbar('northoutside');
axis equal; xlim([fx(1),fx(end)]); ylim([fy(1),fy(end)]);
xlabel('$f_x$ [Hz]'), ylabel('$f_y$ [Hz]')
title('PSD of v-error [dB]')


sgtitle('\textbf{Local Error}')

set(h, 'Units', 'normalized', 'OuterPosition', [.28 .11 .44 .78]);

fileName = 'localError';
printFigure(h, fileName)

