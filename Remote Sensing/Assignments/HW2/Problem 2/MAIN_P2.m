
close all
clear all
clc

set(0,'defaultTextInterpreter','latex')
fontSize = 16;
myCMap = bone;
linesCMap = lines(10);
set(0,'DefaultAxesFontSize', fontSize);

bitmapres = 2000;
ppi = bitmapres/8;


inputImg = 'duomo.png';
[~,outFolder,~] = fileparts(inputImg);
if ~exist(outFolder, 'dir')
       mkdir(outFolder)
end

SAR_data = Generate_raw_SAR_data(inputImg);
axis equal
axis tight

h = gcf;
h_ax = gca;
fileName = sprintf('%s/input_img', outFolder);
printFigure(h, fileName, 'png', bitmapres, ppi)


% % To copy the figure resulting from a binary compiled .p code
% % into a new figure of our own
% h = gcf;
% h_ax = gca;
% 
% h_new = figure;
% subplot(1,2,1)
% new_ax = gca;
% copyobj(flipud(get(h_ax, 'children')), new_ax);
% axis tight
% axis equal
% new_ax.YDir = 'reverse'
% colormap(bone)
% 
% subplot(1,2,2)
% imagesc()
% axis equal
% axis tight



%% RAW SAR DATA & PARAMETERS
D_raw = SAR_data.D; % Raw data matrix [t=fast_time, x_ant=along_track_position of antenna]
D_raw = D_raw / max(max(abs(D_raw)));

t = SAR_data.t_ax; % Fast time vector [s]
x_ant = SAR_data.xa; % Along track position of orbiting antenna [m]
g = SAR_data.g; % Transmitted pulse (complex)

R = SAR_data.r_ax; % Target range axis [m]
x_targets = SAR_data.x_ax; % Target azimuth axis [m]

f0 = SAR_data.f0; % Carrier frequency [Hz]
As = SAR_data.As; % Sinthetic aperture length [m]
B = SAR_data.B; % Bandwidth [Hz]




dt = t(2)-t(1)
dx_ant = x_ant(2)-x_ant(1) % [m] Spatial sampling of the synthetic aperture
Lx = dx_ant * 2 / 0.5
c = 3e+8;   lambda = c / f0

rho_R = c / (2*B) % Range resolution
rho_x = Lx / 2 % Azimuth resolution
delta_psi = lambda / Lx
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Raw data matrix
h = figure;
imagesc(x_ant*1e-3, t*1e+3, abs(D_raw));
colormap(myCMap); shading interp;
xlabel('Along track position of antenna [km]'); ylabel('Fast time [ms]');
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{raw} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
title(sprintf('Raw SAR data'))

fileName = sprintf('%s/raw_data', outFolder);
set(h, 'Units', 'normalized', 'OuterPosition', [0.18 0.3 0.64 0.6]);
printFigure(h, fileName, 'png', bitmapres, ppi)





%% RANGE COMPRESSED DATA (i.e. AFTER MATCHED FILTERING)

N_x_ant = length(x_ant);
N_R = length(R);

D_RC = zeros(size(D_raw));

for j = 1:N_x_ant % loop over antenna position (<--> slow time)
%     crossCorrel = xcorr( D_raw(:,j), g);
%     idx_half = floor(size(crossCorrel)/2); idx_half = idx_half(1)+1;
%     crossCorrel = crossCorrel(idx_half:end); 
%     D_RC(:,j) = crossCorrel;
    D_RC(:,j) = conv(D_raw(:,j),g(:)','same');
end



% % Handler for correlating 2 signals {x,y}
% compress = @(x,y,nfft)(ifft(conj(fft(x,nfft)).*fft(y,nfft)));
% % Compress each range line
% fft_size = 2^nextpow2(length(t));
% for j = 1:N_x_ant % loop on Radar position (<--> slow time)
%     j
%     aux = compress(g,D_raw(:,j),fft_size);      % Correlation
%     D_RC(:,j) = aux(1:size(D_RC,1));  % Save result for line k
% end


h = figure;
imagesc(x_ant*1e-3, R*1e-3, abs(D_RC));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{RC} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
title(sprintf('Range compressed SAR data'))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fileName = sprintf('%s/RC_data', outFolder);
set(h, 'Units', 'normalized', 'OuterPosition', [0.18 0.3 0.64 0.6]);
printFigure(h, fileName, 'png', bitmapres, ppi)


%% FOCUSING BY 1D MATCHED FILTERING

tic
D_foc = zeros(size(D_raw));
Ns = round(As/2/dx_ant);
x_filter = (-Ns:Ns)*dx_ant;
for i = 1:N_R % loop over range values (<--> fast time)
    R_filt_i = sqrt(x_filter.^2 + R(i).^2);
    azimuthFilter = exp(1i*4*pi/lambda*R_filt_i);
    D_foc(i,:) = conv(D_RC(i,:),azimuthFilter,'same');
end
cpuTime_1DMF = toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scalingFact = 1e-3;

h = figure;
subplot(3,2,[1 2])
imagesc(x_ant*1e-3, R*1e-3, abs(D_foc));
colormap(myCMap); shading interp;
axis equal
axis tight
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{focused} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
azMin = x_targets(1);
azMax = x_targets(end);
RMin = R(1);
RMax = R(end);
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
hold on
zoom_1 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(1,:), 'LineWidth', 2);
zoom_1();
title(sprintf('Focused data (1D-azimuth matched filtering)'))



subplot(3,2,[3 5])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
zoom_1();
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{focused} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
azMin = -.095*1e+3;
azMax = azMin + 4*rho_x;
RMin = 700.090*1e+3;
RMax = RMin + 4*rho_R;
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
zoom_2 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2);
title(sprintf('Target region'))
zoom_2();


subplot(3,2,[4])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
% rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
%     'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
zoom_2();
title('$4 \rho_x \times 4 \rho_R$ close up')



subplot(3,2,[3 5])
azMin = -.15*1e+3;
azMax = azMin + 4*rho_x;
RMin = 700.245*1e+3;
RMax = RMin + 4*rho_R;
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
zoom_3 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(3,:), 'LineWidth', 2);
title(sprintf('Target region'))
zoom_3();

subplot(3,2,[6])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
% rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
%     'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
zoom_3();
title('$4 \rho_x \times 4 \rho_R$ close up')



fileName = sprintf('%s/focused_data_1DMatchedFilt', outFolder);
set(h, 'Units', 'normalized', 'OuterPosition', [0.15 0.0 0.7 1.0]);
printFigure(h, fileName, 'png', bitmapres, ppi)



%% Transmitted waveform
% Transmitted waveform (chirp)


padding_steps = 100;
t_chirp = dt*[[1:padding_steps], [padding_steps+1:padding_steps+length(g)],...
    [padding_steps+length(g)+1:2*padding_steps+length(g)]];
g_plot = [zeros(1,padding_steps), g, zeros(1,padding_steps)];

h = figure;
subplot(2,2,1)
plot(t_chirp*1e+3, abs(g_plot))
xlabel('Fast time [ms]'); title('$|g(t)|$');
ylim([-.1, 1.1])
subplot(2,2,3)
plot(t_chirp*1e+3, angle(g_plot))
xlabel('Fast time [ms]'); title('$\angle(g(t))$');


Nt = length(g_plot);
G = fftshift(fft(g_plot)); 
% frequency axis
f_ax = (-(Nt-1)/2:(Nt-1)/2)/Nt/dt;

% compensation of the delay in Matlab's fft implementation
tau = -t(1);
G = G.*exp(1i*2*pi*f_ax*tau);

subplot(2,2,2), plot(f_ax/1e6,abs(G)), grid
xlabel('frequency [MHz]'), title('$|G(f)|$')
subplot(2,2,4), plot(f_ax/1e6,angle(G)), grid
xlabel('frequency [MHz]'), title('$\angle(G(f))$')


sgtitle(sprintf('\\textbf{Transmitted waveform (chirp)}'))



fileName = 'transmittedWaveform';
set(h, 'Units', 'normalized', 'OuterPosition', [0.18 0.3 0.64 0.6]);
h.Renderer = 'Painter';
printFigure(h, fileName)



return

%% FOCUSING BY TIME DOMAIN BACK PROJECTION (TDBP)
tic
[R_grid, x_grid] = ndgrid(R, x_ant);
wbar = waitbar(0,'Backprojecting');
D_foc = zeros(N_R,N_x_ant);
for n = 1:N_x_ant
    waitbar(n/N_x_ant,wbar)
    Rn_xR = sqrt( (x_grid-x_ant(n)).^2 + R_grid.^2);
    % to speed-up the computation, only the samples within a synthetic 
    % aperture are computed
    idx_x_ant = max(1,n-Ns):min(N_x_ant,n+Ns);
    Rn_xR = Rn_xR(:,idx_x_ant);
    Sn = zeros(N_R,N_x_ant); % n-th sensor
    Sn(:, idx_x_ant) = interp1(R,D_RC(:,n),Rn_xR).*exp(+1i*4*pi/lambda*Rn_xR);
    D_foc = D_foc + Sn;
end
close(wbar)
cpuTime_TDBP = toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


scalingFact = 1e-3;

h = figure;
subplot(3,2,[1 2])
imagesc(x_ant*1e-3, R*1e-3, abs(D_foc));
colormap(myCMap); shading interp;
axis equal
axis tight
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{focused} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
azMin = x_targets(1);
azMax = x_targets(end);
RMin = R(1);
RMax = R(end);
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
hold on
zoom_1 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(1,:), 'LineWidth', 2);
zoom_1();
title(sprintf('Focused data (TDBP)'))



subplot(3,2,[3 5])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
zoom_1();
% hcb = colorbar('northoutside');
% set(get(hcb,'Title'),'String','$ | I_\mathrm{focused} | $ [-]', 'Interpreter', 'latex', 'FontSize', fontSize);
azMin = -.095*1e+3;
azMax = azMin + 4*rho_x;
RMin = 700.090*1e+3;
RMax = RMin + 4*rho_R;
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
zoom_2 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2);
title(sprintf('Target region'))
zoom_2();


subplot(3,2,[4])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
% rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
%     'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
zoom_2();
title('$4 \rho_x \times 4 \rho_R$ close up')



subplot(3,2,[3 5])
azMin = -.15*1e+3;
azMax = azMin + 4*rho_x;
RMin = 700.245*1e+3;
RMax = RMin + 4*rho_R;
[~, idx_hZoom_min]  = min(abs(x_ant-azMin));
[~, idx_hZoom_max]  = min(abs(x_ant-azMax));
[~, idx_vZoom_min]  = min(abs(R-RMin));
[~, idx_vZoom_max]  = min(abs(R-RMax));
zoom_3 = @() rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
    'Curvature',0, 'EdgeColor', linesCMap(3,:), 'LineWidth', 2);
title(sprintf('Target region'))
zoom_3();

subplot(3,2,[6])
imagesc(x_ant(idx_hZoom_min:idx_hZoom_max)*scalingFact, R(idx_vZoom_min:idx_vZoom_max)*scalingFact,...
    abs(D_foc(idx_vZoom_min:idx_vZoom_max,idx_hZoom_min:idx_hZoom_max)));
colormap(myCMap); shading interp;
xlabel('Azimuth [km]'); ylabel('Slant range [km]');
axis equal
axis tight
hold on
% rectangle('Position', scalingFact*[x_ant(idx_hZoom_min) R(idx_vZoom_min) (x_ant(idx_hZoom_max)-x_ant(idx_hZoom_min)) (R(idx_vZoom_max)-R(idx_vZoom_min))],...
%     'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
zoom_3();
title('$4 \rho_x \times 4 \rho_R$ close up')



fileName = sprintf('%s/focused_data_TDBP', outFolder);
set(h, 'Units', 'normalized', 'OuterPosition', [0.15 0.0 0.7 1.0]);
printFigure(h, fileName, 'png', bitmapres, ppi)
