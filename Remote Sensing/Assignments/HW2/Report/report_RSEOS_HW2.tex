\input{mySettings}

%\includeonly{chapters/ch02}

\usepackage[english]{babel}


%% *************************** SPECIFIC NEWCOMMANDS ***************************
\newcommand{\parallelsum}{\mathbin{\!/\mkern-5mu/\!}}
% Parallel symbol


\newcommand*\circled[1]{\tikz[baseline=(char.base)]{%
            \node[circle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}
\newcommand*\rectangled[1]{\tikz[baseline=(char.base)]{%
            \node[rectangle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}

\newcommand{\example}[1]{\myhang{\circled{\textbf{\uline{Ex}}}\hspace{4ex}}#1}

\newcommand{\finding}[1]{\myhang{\rectangled{\textbf{\uline{RMK.:}}}\hspace{4ex}}#1}
\newcommand{\numof}[0]{n^{\circ}}

\newcommand{\restr}[2]{\left.#1\right|_{#2}}

\newcommand{\acron}[2]{\noindent\hbox to 0.125\textwidth{\textbf{#1}}#2}

\newcommand{\EV}[1]{E \brackg{#1}}

\newcommand{\estimator}[0]{\V{\hat{x}}}

\newcommand{\FT}[1]{\mathcal{F}\brackg{#1}}
\newcommand{\IFT}[1]{\mathcal{F}^{-1}\brackg{#1}}

\newcommand{\std}[1]{\text{\sffamily{std}}\parg{#1}}

\newcommand{\shv}[0]{\sigma\smallsub{HV}}
\newcommand{\shh}[0]{\sigma\smallsub{HH}}
\newcommand{\snr}[0]{\mathrm{SNR}}


\newcommand\xdownarrow[1][2ex]{%
   \mathrel{\rotatebox{90}{$\xleftarrow{\rule{#1}{0pt}}$}}
}
\newcommand\xDownarrow[1][2ex]{%
   \mathrel{\rotatebox{90}{$\xLeftarrow{\rule{#1}{0pt}}$}}
}

\newcommand{\TeaserImage}[1]{\raisebox{%
  .5\dimexpr-\height+\ht\strutbox-\dp\strutbox}{%
  \includegraphics[height=20ex]{#1}}}


%% *************************** SPECIFIC NEWCOMMANDS ***************************


\graphicspath{{img/}} % Set images directory




\begin{document}

\begin{center}
\includegraphics[width=0.2\textwidth]{logoPolimi_new} \\ \vspace{2ex}
\Huge{\textbf{\sffamily Homework Assignment No. 2}} \\ \Large{\emph{Remote Sensing for Earth Observation \& Surveillance}} \\ \vspace{2ex} \Large{Massimo Piazza} (919920) \\ \mailto{massimo.piazza@mail.polimi.it}
\end{center}


%\subsection*{\centering Abstract}
%\blindtext[1]
 

%\newpage
\let\cleardoublepage\clearpage % In order to avoid a blank page after nomenclature

\vspace{10ex}

\tableofcontents

\thispagestyle{empty}


\newpage

\frontmatter

\subsection*{\centering \huge{Nomenclature}}

\nomenclature[A]{$\sigma$}{Backscattering coefficient \nomunit{dB}}
\nomenclature[A]{$p$}{AGB fitting exponent \nomunit{-}}
\nomenclature[A]{$\snr$}{Signal to Noise Ratio \nomunit{dB}}
\nomenclature[A]{$u$}{Combined uncertainty \nomunit{-}}
\nomenclature[A]{$N_\mathrm{eq}$}{No. equivalent independent samples \nomunit{-}}
\nomenclature[A]{$L_\mathrm{atm}$}{Atmospheric loss \nomunit{dB}}
\nomenclature[A]{$L$}{Length of the averaging window \nomunit{-}}
\nomenclature[A]{$G$}{Antenna gain \nomunit{dB}}
\nomenclature[A]{$f_\mathrm{dir}$}{Antenna directional function \nomunit{-}}
\nomenclature[A]{$I$}{Pixel value \nomunit{-}}
\nomenclature[A]{$\gamma$}{Interferometric coherence \nomunit{-}}




\nomenclature[B]{$z$}{Terrain topography \nomunit{m}}
\nomenclature[B]{$z_\mathrm{amb}$}{Height of ambiguity \nomunit{m}}
\nomenclature[B]{$\phi$}{Interferometric phase \nomunit{deg}}
\nomenclature[B]{$t$}{Fast time \nomunit{s}}
\nomenclature[B]{$\tau$}{Slow time \nomunit{s}}
\nomenclature[B]{$\lambda$}{Wavelength \nomunit{m}}
\nomenclature[B]{$f$}{Frequency \nomunit{MHz}}
\nomenclature[B]{$f_0$}{Carrier frequency \nomunit{MHz}}
\nomenclature[B]{$B$}{Bandwidth \nomunit{MHz}}
\nomenclature[B]{$H$}{Orbital height \nomunit{km}}
\nomenclature[B]{$\theta$}{Incidence angle (off-nadir) \nomunit{deg}}
\nomenclature[B]{$\mathrm{AGB}$}{Above Ground Biomass \nomunit{\frac{ton}{hect}}}
\nomenclature[B]{$\rho_x$}{Azimuth resolution \nomunit{m}}
\nomenclature[B]{$\rho_R$}{Range resolution \nomunit{m}}
\nomenclature[B]{$R$}{Slant range \nomunit{km}}
\nomenclature[B]{$T_n$}{Noise temperature \nomunit{K}}
\nomenclature[B]{$A$}{AGB fitting coefficient \nomunit{\frac{hect}{ton}}}
\nomenclature[B]{$T_\mathrm{avg}$}{Duration of the averaging window \nomunit{\mu s}}
\nomenclature[B]{$L_x$}{Antenna azimuth-wise length \nomunit{m}}
\nomenclature[B]{$L_z$}{Antenna zenith-wise length \nomunit{m}}
\nomenclature[B]{$\Delta x_\mathrm{ant}$}{Spatial sampling of synthetic aperture \nomunit{m}}
\nomenclature[B]{$v_\mathrm{orb}$}{Orbital velocity \nomunit{m/s}}
\nomenclature[B]{$R_E$}{Earth's equatorial radius \nomunit{km}}
\nomenclature[B]{$\mu_E$}{Earth's gravitational parameter \nomunit{\frac{km^3}{s^2}}}
\nomenclature[B]{$A_\mathrm{ill}$}{Illuminated area \nomunit{hect}}
\nomenclature[B]{$\Delta \Psi$}{Antenna azimuthal beamwidth \nomunit{deg}}
\nomenclature[B]{$N_0$}{Noise power density \nomunit{J}}
\nomenclature[B]{$E_r$}{Received energy \nomunit{J}}
\nomenclature[B]{$P_r$}{Received power \nomunit{W}}
\nomenclature[B]{$P_t$}{Transmitted power \nomunit{W}}
\nomenclature[B]{$T_\mathrm{obs}$}{Pulse duration \nomunit{\mu s}}
\nomenclature[B]{$c$}{Speed of light in vacuum \nomunit{\frac{m}{s}}}
\nomenclature[B]{$A_\mathrm{ant}$}{Antenna area \nomunit{m^2}}
\nomenclature[B]{$A_s$}{Synthetic aperture length \nomunit{m}}
\nomenclature[B]{$B_n$}{Normal baseline \nomunit{m}}
\nomenclature[B]{$z_s$}{Cell distance normal to slant range \nomunit{m}}
\nomenclature[B]{$s$}{Slant range displacement \nomunit{m}}
%\nomenclature[B]{$$}{ \nomunit{}}


%\nomenclature[C]{c}{Combustion Chamber}
%\nomenclature[C]{p}{Propellant}
%\nomenclature[C]{tot}{Total}

\nomenclature[D]{\acron{SAR}{Synthetic Aperture Radar}}{}
\nomenclature[D]{\acron{RCS}{Radar Cross-Section}}{}
\nomenclature[D]{\acron{RSS}{Root of Sum of Squares}}{}
\nomenclature[D]{\acron{w.r.t.}{with respect to}}{}
\nomenclature[D]{\acron{PRI}{Pulse Repetition Interval}}{}
\nomenclature[D]{\acron{AGB}{Above Ground Biomass}}{}
\nomenclature[D]{\acron{SNR}{Signal to Noise Ratio}}{}
\nomenclature[D]{\acron{CRLB}{Cramer-Rao Lower Bound}}{}
\nomenclature[D]{\acron{TDBP}{Time Domain Back-Projection}}{}



 \setlength{\columnsep}{30pt}
 \begin{multicols}{2}
 \printnomenclature
 \end{multicols}

\newpage

\mainmatter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROBLEM 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Performance \& Design of a\\Spaceborne SAR (BIOMASS mission)}

	\subsection{Introduction}
	The BIOMASS satellite, which is slated for launch in 2022, will be equipped with a SAR antenna operating at P-Band. The primary aim of the mission is that of mapping Earth's forest \textbf{Above Ground Biomass} (AGB). The latter might be computed, based on the measured backscattering coefficient of vegetation at HV polarization, according to the following fitting law:
	\begin{equation}
\label{eq:AGB_fitLaw}
  \mathrm{AGB} = A \cdot \shv^p
\end{equation}

	
\begin{table}[H]
\centering
\caption{Reference parameters for the operating conditions of BIOMASS}
\label{tab:BIOMASS_params}
\begin{tabular}{@{}ccccccccc@{}}
\toprule
$f_0$   & $B$   & $H$    & $\theta$   & $\rho_x$ & $T_n$ & AGB range                                 & $A$                                         & $p$ \\ \midrule\midrule
435 MHz & 6 MHz & 666 km & 28$^\circ$ & 10 m     & 290 K & $250 \div 500 \addunit{\frac{ton}{hect}}$ & $2.4 \cdot 10^6 \addunit{\frac{hect}{ton}}$ & 2.6 \\ \bottomrule
\end{tabular}
\end{table}
	
	
One of the secondary goals of the mission is to map terrain \textbf{topography} exploiting SAR Interferometry: since the backscattering at HH polarization is mostly contributed by trunk-ground double bounce, then the phase center may be assumed to be proportional to terrain topography.

The reference backscattering coefficient is $\shh = -8 \addunit{dB}$, while the perpendicular baseline between two successive orbits is such as to produce a height of ambiguity in the resulting interferograms equal to $z_|amb| = 80 \addunit{m}$.
	
	
	\subsection{Performance Requirements \& Design}
	
The two fundamental performance requirements that the satellite must guarantee throughout its operating life are:
\begin{enumerate}[label=\roman*)]
	\item relative accuracy\footnote{which is defined as the standard deviation of the estimation error, normalized w.r.t the estimation error itself} of 20\%: \qquad $\displaystyle \frac{\sigma\smallsub{AGB}}{\mathrm{AGB}} \leq 0.2$
		\label{item:req1}
	\item $\snr \geq 10$ at HV polarization, across the whole range of AGB values
		\label{item:req2}
	\item topography estimation accuracy of 5 m
		\label{item:req3}
\end{enumerate}

		\subsubsection{Accuracy Requirements}
		\label{subsec:accuracy_requirements}

In order to guarantee the requirement at \ref{item:req1} it has be identified the relationship according to which the uncertainty upon $\sigma\smallsub{HH}$ will propagate to AGB.

In a generic N-variable situation in which a given quantity $Q$ can be expressed as a function $Q=f(x_1,x_2,\dots,x_N)$, its combined uncertainty can computed from the following RSS:

\begin{equation}
\label{eq:RSS}
u_Q = \sqrt{ \sum_{i=1}^N \pargg{\pder{Q}{x_i}}^2 }
\end{equation}

Due to our simplified single-variable fitting law $\mathrm{AGB} = \mathrm{AGB}(\shv)$, Eq. \ref{eq:RSS} will reduce to:

\begin{equation*}
\begin{split}
u\smallsub{AGB} &= \pder{\mathrm{AGB}}{\shv} u_{\shv} \\
&= A p \shv^{p-1} \cdot u_{\shv}
\quad
\longrightarrow
\quad
\std{\mathrm{AGB}} = Ap\shv^p \frac{\std{\shv}}{\shv}
\end{split}
\end{equation*}

Thus
\begin{equation*}
\begin{split}
\frac{\std{\shv}}{\shv}
&\leq
\min_{\mathrm{AGB} \in [250, 500]}
\curlygg{
\mathrm{AGB} \cdot \underbrace{\frac{\std{\mathrm{AGB}}}{\mathrm{AGB}}}_{0.2} \cdot \frac{1}{Ap\shv^p}
}\\
&= 
\min_{\mathrm{AGB} \in [250, 500]}
\curlygg{
0.2 \cdot \mathrm{AGB} \frac{1}{Ap \underbrace{ \brackg{\shv(\mathrm{AGB})}^p}_{\frac{\mathrm{AGB}}{A}}}
}
\end{split}
\end{equation*}

\begin{equation}
\label{eq:uncertPropag}
\Longrightarrow
\frac{\std{\shv}}{\shv}
\leq
\frac{0.2}{p} \equiv 7.69 \%
\end{equation}


Eq. \ref{eq:uncertPropag} is hence highlighting that the RCS of the various resolution cells should be estimated with an accuracy that is on average better that 7.69\%.

It can be immediately seen that without averaging, it is not possible to achieve such performance. Indeed, if we perform variance estimation using a single pixel ($I \sim \mathcal{CN}(0, \sigma^2)$) $\rightarrow \hat{\sigma}^2 = |I|^2 $. If instead one performs averaging over L neighboring pixels, it is obtained
\begin{equation}
\label{eq:varianceEstim_avg}
\hat{\sigma}^2 = \frac{1}{L} \sum_{l=1}^L |I_l|^2
\end{equation}

It can be derived that variance is proportional to the backscattering coefficient, which implies $\hat{\sigma}^2 \propto \shv \quad \rightarrow \quad \std{\shv} = \displaystyle\frac{\shv}{\sqrt{N_|eq|}}$, from which we can easily compute
\[
N_|eq| = \pargg{ \underbrace{ \frac{\shv}{\std{\shv}} }_{1/7.69\%} }^2 = 169
\]

The corresponding duration of the window in the time domain is then:
\[
T_|avg| = \frac{N_|eq|}{B} = 28.167 \addunit{\mu s}
\]


\subsubsection{Pulse Pattern \& Power Requirements}
The average orbital velocity may be computed by assuming a circular orbit as
\[
v_|orb| = \sqrt{\frac{\mu\smallsub{E}}{R\smallsub{E}+H}} = 7555 \addunit{\frac{m}{s}}
\]

Then, by setting the spatial sampling step-size to $\Delta x_|ant| = 0.5 \frac{L_x}{2}$, one may pick a PRI of the \emph{chirp} waveform equal to
\begin{equation}
\label{eq:PRI}
\mathrm{PRI} = \frac{\Delta x_|ant|}{v_|orb|} = 661.8 \addunit{\mu s}
\end{equation}

For what concerns the pulse duration, its value has been chosen as a result of an iterative procedure, aimed at guaranteeing both the requested SNR (see \ref{item:req2}) and a power level that is reasonable for a spaceborne platform,\footnote{the overall power budget of a big spacecraft is typically in the order of 1 kW} which eventually led to
\[
T_|obs| = 100 \addunit{\mu s}
\]

We will now present the procedure and assumptions used for estimating the power budget of the SAR instrument.

Assuming to have a purely flat illuminated footprint, the corresponding area may be approximately computed as
\begin{equation}
\label{eq:illumArea}
  A_|ill| = R \cdot \underbrace{\rho_R}_{\displaystyle \frac{c}{2B}} \cdot \underbrace{\Delta\Psi}_{\displaystyle \frac{\lambda}{L_x}} = 65.03 \addunit{hect}
\end{equation}

Then for a chirp\footnote{which has constant amplitude} we have:

\begin{equation*}
\begin{split}
\snr = \frac{E_r}{T_|obs|} \qquad \longrightarrow \qquad
E_r &= N_0 \cdot \snr = 7.985 \cdot 10^{-20} \addunit{J}\\
P_r &= \frac{E_r}{T_|obs|} = 7.985 \cdot 10^{-15} \addunit{W}
\end{split}
\end{equation*}


At this point one may compute the power to transmit such as to guarantee the desired SNR in the most critical condition, namely in correspondence of the lowest value of RCS that will be expectedly encountered (AGB$_|min| = 250 \addunit{ton/hect}$).

\[
P_t = P_r \cdot \frac{(4\pi R^2)^2}{\displaystyle\frac{G}{L_|atm|} \cdot f^2_|dir|(\theta,\Psi) \cdot A_|ant| \cdot \mathrm{RCS}_|min|}
\]


\begin{itemize}
	\item $A_|ant| = L_x \cdot L_z$ (where, assuming a typical aspect ratio of 1:7 for the antenna shape $\rightarrow L_z = 2.86 \addunit{m}$)
	\item $\displaystyle G = \frac{4\pi}{\lambda^2} A_|ant|$
	\item $L_|atm| = 3 \addunit{dB}$ (typical value)
	\item $\displaystyle \mathrm{RCS}_|min| = \shv A_|ill| = \pargg{\frac{\mathrm{AGB}_|min|}{A}}^{1/p}A_|ill|$
	\item $f^2_|dir|(\theta, \Psi) \approx 1 $ (for simplicity)
\end{itemize}


This eventually leads to the following simplified equation

\begin{equation}
\label{eq:transmPower}
  P_t = P_r \cdot \frac{(4\pi R^2)^2}{\displaystyle\frac{G}{L_|atm|} \cdot A_|ant| \cdot \pargg{\frac{\mathrm{AGB}_|min|}{A}}^{1/p}\cdot A_|ill|}
   = 493.8 \addunit{W}
\end{equation}


		\subsubsection{Further considerations}
		If we were to perform a more accurate analysis and account for the presence of \emph{thermal noise}, then the results obtained in \ref{subsec:accuracy_requirements} would differ. The backscattering from vegetation would have to be treated as a random process, which indeed changes how the uncertainty affecting $\shv$ will propagate to the uncertainty of AGB, given that $\shv = \shv\smallsup{vegetation} + \shv\smallsup{\text{thermal noise}}$ and $\mathrm{AGB} = A \cdot \shv^p$. It is intuitive that this would eventually result in a stricter requirement upon the relative accuracy of $\shv$, compared to what computed previously.




		
	\subsection{InSAR Applications}
Topography estimation by means of InSAR measurements is based on computing the interferogram between two radar images, taken from slightly different look angles. The resulting interferogram will then undergo \emph{flattening} and \emph{phase unwrapping}, eventually producing a map of terrain altitude.

Given two generic images $\M{I}_m$, $\M{I}_n$ their interferogram is computed as the element-wise multiplication
\begin{equation}
\label{eq:interferogram}
  \mathrm{interferogram} = \M{I}_m \circ \,\, \M{I}_n^*
\end{equation}

		\subsubsection{Phase Noise Estimation}
The interferometric coherence at a generic pixel $I$ is defined as
\begin{equation}
\label{eq:interfCoherence_def}
  \gamma := \frac{\EV{I_mI_n^*}}{\sqrt{E^2\brackg{I_m} \cdot E^2\brackg{I_n^*}}}
\end{equation}

In practice, the expected value operator in Eq. \ref{eq:interfCoherence_def} is replaced by spatial averaging, that is carried out by means of a sliding averaging window that replaces the value of $\gamma$ at any pixel location with the one obtained by averaging the contribution from a given number of $L$ neighboring pixels. This eventually yields the so called \emph{coherence map}.
Eq. \ref{eq:interfCoherence_def} might also be rewritten as


\begin{equation}
\label{eq:interfCoherence_SNR}
\gamma = \frac{\snr}{1+\snr} e ^ {j\frac{4\pi}{\lambda}(R_m-R_n)}  
\end{equation}


We may at this point estimate the reference value for the received power at HH polarization and thus identify the corresponding SNR:

\begin{equation}
\label{eq:receivedPower}
  P_r = P_t \frac{GA_|ant|}{(4\pi R^2)^2} \underbrace{f^2_|dir|(\theta, \Psi)}_{\displaystyle \approx 1} \cdot \underbrace{\mathrm{RCS}}_{\displaystyle \shh A_|ill|}
  = 4.3045 \cdot 10^{-14} \addunit{W}
\end{equation}

\begin{equation}
\label{eq:SNR}
  \snr = \frac{E_r}{N_0} = \frac{P_r T_|obs|}{N_0} =  53.907
\end{equation}


Hence, using Eq. \ref{eq:interfCoherence_SNR}: $\displaystyle |\gamma| = \frac{\snr}{1+\snr} = 0.982$.

Although lacking any empirical dataset for estimating phase noise, we may still resort to a theoretical evaluation. Indeed, it can be derived that the CRLB of phase noise\footnote{i.e. the lowest achievable dispersion} will exhibit the following dependence upon the number of pixels in the averaging window

\begin{equation}
\label{eq:phaseDispers_CRLB}
\sigma^2\smallsub{\Delta\phi} = \frac{1-|\gamma|^2}{2L|\gamma|^2} = \frac{ 0.1368}{L}
\end{equation}


		\subsubsection{Accuracy Requirements}
In the event of still targets, the interferometric phase variation before flattening is given by

\begin{equation}
\label{eq:phaseDiff_BF}
\begin{split}
\Delta \phi\smallsup{BF} &= \frac{4\pi}{\lambda} \frac{B_n z_s}{R}\\
&= -\frac{4\pi B_n}{\lambda R \sin \theta} z - 
\frac{4\pi B_n s}{\lambda R \tan \theta}
\end{split}
\end{equation}


The flattening procedure simply consists in computing the second term in Eq. \ref{eq:phaseDiff_BF} and removing it, which eventually leaves us with

\begin{equation}
\label{eq:phaseDiff}
  \Delta \phi = - \underbrace{\frac{4\pi B_n}{\lambda R \sin \theta}}_{\displaystyle \equiv \frac{2\pi}{z_|amb|}} z
\end{equation}

From Eq. \ref{eq:phaseDiff} it follows that
\[
\sigma\smallsub{\Delta \phi} = \frac{2\pi}{z_|amb|} \sigma_z = 2\pi \frac{5}{80} = 0.3927 \addunit{rad}
\]

and by solving Eq. \ref{eq:phaseDispers_CRLB} for L:

\begin{equation}
\label{eq:min_window_size}
L = \texttt{ceil} \pargg{\frac{1-|\gamma|^2}{2 \sigma^2\smallsub{\Delta \phi} |\gamma|^2}} = 1 
\end{equation}

In other words, it turns out that even without averaging the desired 5 m accuracy is still guaranteed.












	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROBLEM 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Focusing of Raw SAR Data}

	\subsection{Introduction}
For the development and benchmarking of a focusing algorithm for raw SAR data, the latter have been synthesized by means of an algorithm that takes as input a generic digital image.
Data synthesis is based on a simulated spaceborne system operating at:

\begin{table}[H]
\centering
\caption{Reference parameters for raw SAR data synthesis}
\label{tab:SAR_synth_params}
\begin{tabular}{@{}ccc@{}}
\toprule
$f_0$ & $B$    & $A_s$  \\ \midrule\midrule
5 GHz & 50 MHz & 7005 m \\ \bottomrule
\end{tabular}
\end{table}

Along with the synthesized data, the following dataset is provided as well

\begin{table}[H]
\centering
\caption{SAR dataset}
\label{tab:datasetP1}
\begin{tabular}{@{}ccc@{}}
\toprule
                                          & \textbf{Variable Name} & \textbf{Variable Size} \\ \midrule\midrule
Raw data matrix                           & \texttt{D}             & $535 \times 3003$      \\
Fast time axis                            & \texttt{t_ax}          & $1 \times 535$         \\
Along track positions of orbiting antenna & \texttt{xa}            & $1 \times 3003$        \\
Transmitted pulse                         & \texttt{g}             & $1 \times 201$         \\
Range positions of targets                & \texttt{r_ax}          & $1 \times 535$         \\
Azimuth positions of targets              & \texttt{x_ax}          & $1 \times 334$         \\ \bottomrule
\end{tabular}
\end{table}


For the upcoming discussion, data generated from an aerial view of Milan (Fig. \ref{fig:input_img}) will be taken as a reference.

\begin{figure}[H]
\centering
	\begin{subfigure}[H]{0.49\textwidth}
	\includegraphics[width=\textwidth]{input_img}
	\caption{\label{fig:input_img} Input image (aerial view of Milan: 45.46N, 9.19E)}
	\end{subfigure}
	\begin{subfigure}[H]{0.49\textwidth}
	\includegraphics[width=\textwidth]{raw_data}
	\caption{\label{fig:raw_data} Synthesized raw SAR data}
	\end{subfigure}
\caption{\label{fig:input_and_rawData} Input and corresponding synthesized data}
\end{figure}

\newpage

	\subsection{Focusing Algorithm}

SAR focusing basically consists in two macro-steps:
\begin{enumerate}
	\item \textbf{range compression}, which is achieved by convolving\footnote{it will specifically consist in a 1D same-convolution} each radar echo with the transmitted chirp waveform; we will hence loop over the columns of the raw data matrix (each of which is associated with a different along-track position of the antenna along its orbit)
	\begin{equation}
	\label{eq:D_RC}	
	\texttt{D}_|\texttt{RC}|\texttt{(:,j)} \texttt{ = conv(D}_|\texttt{raw}|\texttt{(:,j), g(:), 'same')}
	\qquad \forall j = 1,...N_{x_a}
	\end{equation}
	
	
	\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{transmittedWaveform}
	\caption{\label{fig:transmittedWaveform} Transmitted waveform both in time and frequency domain}
	\end{figure}

	

	\item \textbf{azimuth compression}, which might be performed using several available techniques, the simplest one being 1D matched filtering
	\begin{equation}
	\label{eq:D_foc}
	\texttt{D}_|\texttt{foc}|\texttt{(i,:)} \texttt{ = conv(D}_|\texttt{RC}|\texttt{(i,:), } \exp\pargg{+j \frac{4\pi}{\lambda} \V{R}_i\smallsup{filt}}\texttt{, 'same')}
	\qquad \forall i = 1,...N_{R}
	\end{equation}
	

	where $\V{R}_i\smallsup{filt} = \sqrt{\V{x}_|filt|^2 + \V{R}_i^2}$.
	
\end{enumerate}


For azimuth compression it was chosen to employ 1D matched filtering, given its much lower computational cost compared to other more advanced algorithms such as TDBP. In addition, it can be experienced that in this situation the focused images produced by matched filtering and TDBP are close to identical.

Just to provide a comparison, the CPU time\footnote{hardware used: 2.5 GHz Quad-Core, 16 GB 1600 MHz DDR3} it takes for running the two algorithms is reported in Tab. \ref{tab:cpuTime_azCompr}.


\begin{table}[H]
\centering
\caption{Computational cost of the tested azimuth-compression algorithms}
\label{tab:cpuTime_azCompr}
\begin{tabular}{@{}ccc@{}}
\toprule
\textbf{}    & \textbf{1D Matched Filtering} & \textbf{TDBP}                                                           \\ \midrule\midrule
CPU time [s] & 0.6227                        & \begin{tabular}[c]{@{}c@{}}314.78\\ 255.69 (parallel pool)\end{tabular} \\ \bottomrule
\end{tabular}
\end{table}


\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{focused_data_1DMatchedFilt}
\caption{\label{fig:focused_data_1DMatchedFilt} Focused data obtained using 1D matched filtering}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{focused_data_TDBP}
\caption{\label{fig:focused_data_TDBP} Focused data obtained using TDBP}
\end{figure}


As one may easily verify from the close-ups in Fig. \ref{fig:focused_data_1DMatchedFilt}, the resolution of the resulting image is consistent with the theoretical estimation, according to which
\[
\rho_x = \frac{L_x}{2} = 2\Delta x_|ant| = 5 \addunit{m}
\]
\[
\rho_R = \frac{c}{2B} = 3 \addunit{m}
\]


We may then summarize the whole focusing procedure as follows:



%\rotatebox[origin=c]{-90}{\parbox[t]{2cm}{synthesize SAR data}}
%\parbox[t]{2cm}{synthesize SAR data} \xdownarrow[5ex]

\begin{center}
\begin{alignat*}{4}
\TeaserImage{step_0}
&\Longrightarrow
\TeaserImage{step_1}
&&\Longrightarrow
\TeaserImage{step_2}
&&\Longrightarrow
\TeaserImage{step_3}\\
&\parbox[t]{2cm}{synthesize SAR data}   &&\parbox[t]{2cm}{range\\ compression}   &&\parbox[t]{2cm}{azimuth\\ compression}
\end{alignat*}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage

\nocite{*} % IMPORTANT: without this command, bib items which aren't already cited won't show up within your "References"

\printbibliography

\thispagestyle{empty}

\end{document}