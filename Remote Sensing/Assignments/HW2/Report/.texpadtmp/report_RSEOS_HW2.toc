\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Performance \& Design of a\\Spaceborne SAR (BIOMASS mission)}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Introduction}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Performance Requirements \& Design}{1}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.1}Accuracy Requirements}{1}{subsubsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.2}Pulse Pattern \& Power Requirements}{2}{subsubsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.3}Further considerations}{3}{subsubsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}InSAR Applications}{3}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.3.1}Phase Noise Estimation}{3}{subsubsection.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.3.2}Accuracy Requirements}{4}{subsubsection.1.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Focusing of Raw SAR Data}{5}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Introduction}{5}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Focusing Algorithm}{6}{subsection.2.2}
