function [sys,x0,str,ts] = Solar_pressure(t,x,u,flag)

% Model for the Solar Pressure torque
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 9;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = [];

str = [];
ts  = [0  0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)
sys = [];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)
% Altitude of the orbit used to select the values of the radiations:
% R_orbit = 550+6371; %[Km]

%Direct solar radiation 
F_solar = 1358; %[W/m^2]

%Radiation reflected by the Earth
F_refl = 600; %[W/m^2]

%Earth radiation
F_Earth = 150; %[W/m^2]

%Total Radiation force
F_rad = F_solar+F_refl+F_Earth; %[W/m^2]

% Speed of light
c = 3*10^8; %[m/s] 

%Solar radiation pressure (average)
P = F_rad/c; 

%coefficient of the material rho_s+rho_d+rho_a=1
rho_s = 0.6;   %specular reflection coefficient
rho_d = 0.35;  %diffuse reflection coefficient
rho_a = 0.05; %absorption coefficient

%area of the spacecraft
%sides length
L_x = 13;      %[m]
L_y = 13;      %[m]
L_z = 4;     %[m]

%area of the spacecraft
A_x = L_y*L_z;    %[m^2]
A_y = L_x*L_z;    %[m^2]
A_z = pi*(L_z/2)^2;    %[m^2]

%normal vector of the surfaces
N_xp = [1 0 0]';  %x_normal
N_yp = [0 1 0]';  %y_normal
N_zp = [0 0 1]';  %z_normal

N_xn = -N_xp;  %(minus)x_normal
N_yn = -N_yp;  %(minus)y_normal
N_zn = -N_zp;  %(minus)z_normal

%Body-frame rotation matrix
A_BN = [u(1)   u(2)   u(3)  ;
        u(4)   u(5)   u(6)  ;
        u(7)   u(8)   u(9) ]; %input matrix 

%orbit characteristic
mu_Earth =398600; %[Km^3/s^2]
R_orbit = 550+6371; %[Km]
n = sqrt(mu_Earth/R_orbit^3);  %[km/s]

%Sun measurement vector
w_S = n ; 
S_i = [-sin(w_S*t);cos(w_S*t);0]; %Sun direction vector in inertial frame
S_b = A_BN*S_i; %Sun direction vector in body-frame (column vector)

%Definition of the pressure centre (10% around the centre of mass CM)
C_p =[0.1*L_x 0.1*L_y 0.1*L_z]';

%Pressure force

if S_b'*N_xp >0
   F_x = P*A_x*(S_b'*N_xp)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_xp))+(2/3*rho_d))*N_xp); %positive x_direction
   T_spx = cross(C_p,F_x);
else 
   F_x = P*A_x*(S_b'*N_xn)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_xn))+(2/3*rho_d))*N_xn); %negative x_direction 
   T_spx = cross(C_p,F_x);
end


if S_b'*N_yp >0
    F_y = P*A_y*(S_b'*N_yp)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_yp))+(2/3*rho_d))*N_yp); %positive y_direction
    T_spy = cross(C_p,F_y);
else
    F_y = P*A_y*(S_b'*N_yn)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_yn))+(2/3*rho_d))*N_yn); %negative y_direction
    T_spy = cross(C_p,F_y);
end

if S_b'*N_zp >0
    F_z = P*A_z*(S_b'*N_zp)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_zp))+(2/3*rho_d))*N_zp); %positive z_direction
    T_spz = cross(C_p,F_z);
else 
    F_z = P*A_z*(S_b'*N_zn)*((1-rho_s)*S_b+((2*rho_s*(S_b'*N_zn))+(2/3*rho_d))*N_zn); %negative y_direction
    T_spz = cross(C_p,F_z);
end

%total solar pressure acting on the spacecraft
T_SP = T_spx+T_spy+T_spz; 
y(1) = T_SP(1);
y(2) = T_SP(2);
y(3) = T_SP(3);

sys = y;