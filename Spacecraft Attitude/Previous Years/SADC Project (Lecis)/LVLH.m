function [sys,x0,str,ts] = LVLH(t,x,u,flag)

%LVLH frame 
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 12;
sizes.NumInputs      = 12;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = []';

str = [];
ts  = [0  0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)

sys = [];

% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

mu_Earth =398600; %[Km^3s^-2]
R_orbit = 550+6371; %[Km]
n = sqrt(mu_Earth/R_orbit^3); %orbital velocity

w_LN = [0 0 n]';          %desidered velocity of L/N frame

w_BN = [u(1) u(2) u(3)]'; %input angular velocity arriving from Euler equations

%Rotation matrix 
A_BN = [u(4)   u(5)   u(6)  ;
        u(7)   u(8)   u(9)  ;
        u(10)  u(11)  u(12)]; %input matrix 
   
A_LN = [cos(n*t) sin(n*t) 0 ;
       -sin(n*t) cos(n*t) 0 ;
           0        0     1];  %matrix in L/N frame(desidered attitude matrix for pointing) = A_d
  
A_BL = A_BN*A_LN';     %error matrix: A_err = A_BL

w_BL = w_BN - A_BL*w_LN; %error vector on the w desidered

%definition of the outputs
y(1,1) = w_BL(1);
y(1,2) = w_BL(2);
y(1,3) = w_BL(3);

y(1,4) = A_BL(1,1);  
y(1,5) = A_BL(1,2); 
y(1,6) = A_BL(1,3); 

y(1,7) = A_BL(2,1);  
y(1,8) = A_BL(2,2);
y(1,9) = A_BL(2,3);  

y(1,10) = A_BL(3,1);  
y(1,11) = A_BL(3,2);  
y(1,12) = A_BL(3,3);   


sys = y;

% end mdlOutputs