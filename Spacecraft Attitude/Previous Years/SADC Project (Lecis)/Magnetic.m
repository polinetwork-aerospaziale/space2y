function [sys,x0,str,ts] = Br(t,x,u,flag)

% magnetic field model
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 9;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = []';

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)
%define variables and parameters
sys = [];


% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)


% the form P10 = P(n-1,m-0), P11 = P(n-1,m-1)
% dP The partial derivative of P with respect to theta
%number of iterations


mu =398600; %[Km^3s^-2]
R = 550+6371; %[Km]
n = sqrt(mu/R^3); %[Km/s]
i = 28.47*pi/180;
w_e = 0.000072921;

x = R*[cos(n*t); sin(n*t)*cos(i); sin(n*t)*sin(i)];
theta = pi/2 - acos(x(3)/r);
psi = atan(x(2)/x(1)) - w_e*t;



A_BN(1,1) = u(1);
A_BN(1,2) = u(2);
A_BN(1,3) = u(3);
A_BN(2,1) = u(4);
A_BN(2,2) = u(5);
A_BN(2,3) = u(6);
A_BN(3,1) = u(7);
A_BN(3,2) = u(8);
A_BN(3,3) = u(9);

N=13;

Br=0;
Bth=0;
Bph=0;

P11=1; P10=P11;
dP11=0; dP10=dP11;
for m=0:N
for n=1:N
if m<=n
% Calculate Legendre polynomials and derivatives recursively
if n==m
  
P2 = sin(theta)*P11;
dP2 = sin(theta)*dP11 + cos(theta)*P11;
P11=P2; P10=P11; P20=0;
dP11=dP2; dP10=dP11; dP20=0;
elseif n==1

P2 = cos(theta)*P10;
dP2 = cos(theta)*dP10 - sin(theta)*P10;
P20=P10; P10=P2;
dP20=dP10; dP10=dP2;
else
K = ((n-1)^2-m^2)/((2*n-1)*(2*n-3));
P2 = cos(theta)*P10 - K*P20;
dP2 = cos(theta)*dP10 - sin(theta)*P10 - K*dP20;
P20=P10; P10=P2;
dP20=dP10; dP10=dP2;
end
% Sums to calculate Br (note that real values of a, r, psi, g_ij, h_ij are required as inputs)
a=6371.2;
r=6921;

g=[-29619.4 -1728.20 0 0 0 0 0 0 0 0 0 0 0 0;
    -3401.55 5314.62 1447.04 0 0 0 0 0 0 0 0 0 0 0;
    3349 -7005.54 2424.68 564.86 0 0 0 0 0 0 0 0 0 0;
    4078.81 4354.14 978.28 -842.94 82.31 0 0 0 0 0 0 0 0 0;
    -1723.05 3572.54 1708.42 -613.69 -374.04 -9.05 0 0 0 0 0 0 0 0;
    1043.83 1289.19 1108.86 -1603.02 -32.20 39.32 -60.72 0 0 0 0 0 0 0;
    2118.19 -2624.75 0 681.93 112.38 42.6 17.68 -0.78 0 0 0 0 0 0;
    1226.67 442.41 -515.96 -327.21 -443.82 134.96 48.06 -19.8 -4.39 0 0 0 0 0;
    474.8 1197.59 325.95 -697.06 355.17 -299.85 -26.1 70.06 -11.11 -4.99 0 0 0 0;
    -469.11 -1459.72 358.18 -512.37 -58.44 273.49 41.32 40.09 34.37 0.8 -0.65 0 0 0;
    930.01 -792.06 -777.19 491.95 -23.95 15.84 -65.88 34.72 38.69 0.88 3.26 2.32 0 0;
    -1452.43 -269.11 159.03 584.3 -97.38 300.62 -104.15 35.12 -17.56 -10.22 -0.94 -0.56 -0.23 0;
    -253.92 -1557.16 464.26 128.31 -393.65 904.64 -180.62 187 -57.12 20.42 -2.84 4.01 0 0.06]*10^(-9);

h=[0 5186.1 0 0 0 0 0 0 0 0 0 0 0 0;
    0 -4298.26 -396.64 0 0 0 0 0 0 0 0 0 0 0;
    0 -696.88 568.17 -388.25 0 0 0 0 0 0 0 0 0 0;
    0 1508.56 -907.45 250.58 -224.66 0 0 0 0 0 0 0 0 0;
    0 445.3 1321.09 -626.4 -87.19 74.58 0 0 0 0 0 0 0 0;
    0 -328.91 951.95 648.58 -333.96 1.63 29.42 0 0 0 0 0 0 0;
    0 -2291.34 -700.85 126.97 296.37 91.38 -61.51 -3.75 0 0 0 0 0 0;
    0 797.67 -1205.77 352.07 -574.83 229.87 61.1 -37.35 -1.32 0 0 0 0 0;
    0 -2509.85 1455.91 1037.29 -349.53 -283 146.14 28.63 -21.19 2.92 0 0 0 0;
    0 413.59 0 661.12 572.67 -436.1 -49.58 -58.13 1.64 -5.84 -4.39  0 0 0;
    0 46.64 531.76 -295.17 -622.74 142.58 -65.88 -138.89 -20.48 -10.58 -5.17 -0.52 0 0;
    0 -358.81 238.54 1623.05 -1265.98 263.81 62.49 0 0 7.66 -8.49 -1.11 0.45 0;
    0 -1557.16 309.5 2309.64 -393.65 -695.88 -45.15 187 42.84 40.84 8.52 -2.01 -1.42 -0.5]*10^(-9);

Br = Br + (a/r)^(n+2)*(n+1)*((g(n,m+1)*cos(m*psi) + h(n,m+1)*sin(m*psi))*P2);
Bth = Bth -(a/r)^(n+2)*((g(n,m+1)*cos(m*psi) + h(n,m+1)*sin(m*psi))*dP2);

if theta < 10^(-3)
    sin_theta=10^(-3);
else 
    sin_theta=sin(theta);
end

Bph = Bph -1/sin_theta*(a/r)^(n+2)*m*((-g(n,m+1)*sin(m*psi) + h(n,m+1)*cos(m*psi))*P2);

end

end
end

y1 =Br;
y2 =Bth;
y3 =Bph;
y=[y1; y2; y3];

B = A_BN*y;

sys = [B'];


% end mdlOutputs