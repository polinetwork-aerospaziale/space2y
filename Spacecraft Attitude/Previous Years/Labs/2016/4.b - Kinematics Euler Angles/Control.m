function [sys,x0,str,ts] = Control(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 12;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);
x0=[];



str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)






sys = [];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)
k1 = 1;
k2 = 0.1;
w(1) = u(1);
w(2) = u(2);
w(3) = u(3);
t(1) = u(4);
t(2) = u(5);
t(3) = u(6);
wd(1) = u(7);
wd(2) = u(8);
wd(3) = u(9);
td(1) = u(10);
td(2) = u(11);
td(3) = u(12);
out =-k1*(w-wd)-k2*(t-td);
sys = [out];


% end mdlOutputs