function [sys,x0,str,ts] = Kinematics(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 4;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 4;
sizes.NumInputs      = 3;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

xx0 = [0 0 0 0]';  %satellite strating position in quaternions
d0  = [0 0 0 0]'; 
x0 = xx0 +d0;

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)



%define variables and parameters

A =([    0,    u(3), -u(2), u(1); ...
       -u(3),   0,    u(1), u(2); ...
        u(2), -u(1),   0,   u(3); ...
       -u(1), -u(2), -u(3),  0]);

   
%s Kinematics of Quaternion

qdot = A*x;

sys = [qdot];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs