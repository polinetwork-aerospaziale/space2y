close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

pointing_error_gravityGradient
set_param('pointing_error_gravityGradient', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('pointing_error_gravityGradient')
simOut.getSimulationMetadata.ModelInfo.SolverInfo

%% PLOT
subplot(2,1,1)
plot(simOut.tout,simOut.w_BL.Data)
legend({'$\omega^{BL}_x$', '$\omega^{BL}_y$', '$\omega^{BL}_z$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('$\underline{\omega}^{BL}$ [rad/s]', 'Interpreter', 'latex')

subplot(2,1,2)
plot (simOut.tout,simOut.err.Data)
legend({'trace($A_{B/L} - \mathcal{I}$)'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Attitude Error', 'Interpreter', 'latex')