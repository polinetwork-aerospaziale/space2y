
close all
clear all
clc


% ---------- TRY TO AVOID GLOBAL VARIABLES IN YOUR CODES!!!!---------------
% I used global variables only to show internal optimization steps!!!!!
global history
outtfun = @(x,optimValues,state) outfun(x,optimValues,state);
optSettings = optimoptions(@fmincon,'OutputFcn',outtfun);
% -------------------------------------------------------------------------

% always decleare you objective function as anonymous @(x). the only
% variable shall be x. it can be a vector or a scalar
constant = 3;
single_objective_function = @(x) fun_peaks(x,constant);

% figure('Color',[1,1,1])
for k = 1:5
    history.x = [];
    history.fval = [];
   
    % IGs
    x0{1} = [ -1,-1 ];
    x0{2} = [ 1.5,-1 ];
    x0{3} = [ 2,2 ];
    x0{4} = [ 0,1.5 ];
    x0{5} = [4*(rand(1)-0.5),4*(rand(1)-0.5)];
    
    col{1} = 'r';
    col{2} = 'g';
    col{3} = 'c';
    col{4} = 'm';
    col{5} = 'y';
    
    
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    %nonln = [];
    nonln = @(x) nonlinconstr(x); % Must lie on the unit circle @origin
    LB = [-3,-3];
    UB = [3,3];
   
    % optimization occurs here
    [x{k},fval{k}] = fmincon(single_objective_function,x0{k},A,b,Aeq,beq,LB,UB,nonln,optSettings);
    
    % this is to show the topology
    x_map = [linspace(-3,3,1000);linspace(-3,3,1000)];
    % always initialize your variables
    Map = zeros(1000);
    for i = 1:1000
        for j = 1:1000
            Map(j,i) = fun_peaks([x_map(1,i);x_map(2,j)],constant);
        end
    end
    
    % plot of the iterations
    surf(x_map(1,:),x_map(2,:),Map,'EdgeColor','none')
    if k ==1
        pause
    end
    hold on
    xlabel('x')
    ylabel('y')
    zlabel('z')
    for i = 1:numel(history.fval)
        plot3(history.x(i,1),history.x(i,2),fun_peaks(history.x(i,:),constant)+0.1,'+','MarkerEdgeColor',col{k},'MarkerSize',8,'LineWidth',2)
        if i ==numel(history.fval)
        plot3(history.x(i,1),history.x(i,2),fun_peaks(history.x(i,:),constant)+0.1,'o','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',8,'LineWidth',2)    
        end
        text(history.x(i,1)+.15,history.x(i,2),fun_peaks(history.x(i,:),constant)+01,num2str(i));
        if i ==1
            pause
        end
        history.fval(i)
        s = strcat('fval: ',num2str(history.fval(i)));
        title(s)
        pause(0.5)
        
    end
    
    
end
