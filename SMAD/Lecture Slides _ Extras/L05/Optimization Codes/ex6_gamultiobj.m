
close all
clear all
clc


opt = optimoptions(@gamultiobj,'PlotFcn', @gaplotpareto,'PopulationSize',100,'MaxGenerations',1000,'Display','iter');
% -------------------------------------------------------------------------

% always decleare you objective function as anonymous @(x). The only
% variable shall be x. It may either be a vector or a scalar
constant = 3;
double_objective_function = @(x) fun_2obj(x);


% optimization occurs here
nVars = 2;
LB = [-3,-3];
UB = [3,3];
[x,fval,exitflag,output,population,scores] = gamultiobj(double_objective_function,nVars,[],[],[],[],LB,UB,[],opt);



%%

dist = sqrt(fval(:,1).^2 + fval(:,2).^2);
[M,idx] = min(dist);
[M1,idx1] = min(fval(:,1));
[M2,idx2] = min(fval(:,2));
figure('Color',[1,1,1])
plot(fval(idx,1),fval(idx,2),'o','MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor','k')
hold on
grid on
plot(fval(idx1,1),fval(idx1,2),'o','MarkerSize',8,'MarkerEdgeColor','b','MarkerFaceColor','b')
plot(fval(idx2,1),fval(idx2,2),'o','MarkerSize',8,'MarkerEdgeColor','g','MarkerFaceColor','g')
plot(fval(:,1),fval(:,2),'+','MarkerEdgeColor','r') % All pareto solutions (i.e. they build up the pareto front)
xlabel('First Objective')
ylabel('Second Objective')
title('Pareto Front')
legend('Minimum distance from origin (good compromise?)','Optimum for Objective1','Optimum for Objective2')





